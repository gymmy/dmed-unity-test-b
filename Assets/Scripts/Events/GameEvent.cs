﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class GameEvent : Object
{
    public readonly Object Writer;

    public GameEvent(Object p_writer)
    {
        Writer = p_writer;
    }
}

public sealed class OnPressedButtonEvent : GameEvent
{
    public readonly int ButtonIndex;

    public OnPressedButtonEvent(Object p_writer, int p_buttonIndex) : base(p_writer)
    {
        ButtonIndex = p_buttonIndex;
    }
}
