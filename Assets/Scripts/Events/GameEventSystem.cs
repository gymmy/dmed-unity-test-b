﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public sealed class GameEventSystem : ScriptableObject
{
    private static Dictionary<System.Type, List<UnityAction<GameEvent>>> m_gameEventDictionary = new Dictionary<System.Type, List<UnityAction<GameEvent>>>();

    public static void RaiseEvent<T>(GameEvent p_gameEvent) where T : GameEvent
    {
        List<UnityAction<GameEvent>> _callbacks;
        m_gameEventDictionary.TryGetValue(typeof(T), out _callbacks);
        if (_callbacks != null)
        {
            for (int i = 0; i < _callbacks.Count; ++i)
            {
                _callbacks[i].Invoke(p_gameEvent);
            }
        }
    }

    public static void AddListener<T>(UnityAction<GameEvent> p_callback) where T : GameEvent
    {
        if (m_gameEventDictionary == null)
        {
            return;
        }

        System.Type _type = typeof(T);

        if (m_gameEventDictionary.ContainsKey(_type))
        {
            m_gameEventDictionary[_type].Add(p_callback);
        }
        else
        {
            m_gameEventDictionary.Add(_type, new List<UnityAction<GameEvent>>() { p_callback });
        }
    }

    public static void RemoveListener<T>(UnityAction<GameEvent> p_callback)
    {
        if (m_gameEventDictionary == null)
        {
            return;
        }

        System.Type _type = typeof(T);

        if (m_gameEventDictionary.ContainsKey(_type))
        {
            m_gameEventDictionary[_type].Remove(p_callback);

            // Delete game event key if there are no more listeners
            if (m_gameEventDictionary[_type].Count == 0)
            {
                m_gameEventDictionary.Remove(_type);
            }
        }
    }
}