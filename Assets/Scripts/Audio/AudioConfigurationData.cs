﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Container class for audio clips that are used for each category.
/// </summary>
[CreateAssetMenu(fileName = "AudioConfig", menuName = "ScriptableObjects/CreateAudioConfig", order = 2)]
public class AudioConfigurationData : ScriptableObject
{
    [SerializeField] private List<AudioClip> m_audioClipList;

    public List<AudioClip> GetAudioList() 
    {
        return m_audioClipList;
    }
}