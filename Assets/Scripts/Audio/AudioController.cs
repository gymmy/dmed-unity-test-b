﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

/// <summary>
/// Main controller and manager in charge of playing audio data
/// </summary>
public sealed class AudioController : MonoBehaviour
{
	/// <summary>
	/// Data structure used by AudioController to manage played audio.
	/// </summary>
	private class AudioPlayableData
	{
		public List<AudioClip> ClipsReadyList = new List<AudioClip>();
		public Queue<AudioClip> ClipsCooldownQueue = new Queue<AudioClip>();

		public AudioPlayableData(List<AudioClip> p_clipsReady)
		{
			// Copy by value
			AudioClip[] _outlist = new AudioClip[p_clipsReady.Count];
			p_clipsReady.CopyTo(_outlist);

			for (int i = 0; i < _outlist.Length; ++i)
			{
				ClipsReadyList.Add(p_clipsReady[i]);
			}
		}

		public void Initialize(int p_cooldownLength)
		{
			// set initial 3x cooldown
			if (ClipsCooldownQueue.Count == 0)
			{
				for (int i = 0; i < p_cooldownLength; ++i)
				{
					ClipsCooldownQueue.Enqueue(null);
				}
			}
		}
	}

	[Header("System References")]
	[SerializeField] private MainGameConfiguration m_gameConfiguration;

	[Header("Component References")]
	[SerializeField] private AudioSource m_audioSourceComponent;

	private List<AudioPlayableData> m_audioDataList = new List<AudioPlayableData>();

	private void Awake()
	{
		// Check for null fields
		if (m_audioSourceComponent)
		{
			m_audioSourceComponent = GetComponent<AudioSource>();
		}
		else 
		{
			Debug.Log("[AUDIOMANAGER] Please assign AudioSource component.");
		}

		// Initialize audio data list
		AudioConfigurationData[] _audioConfigurationData;
		m_gameConfiguration.GetAudioConfigurationDatas(out _audioConfigurationData);
		for (int i = 0; i < _audioConfigurationData.Length; ++i) 
		{
			AudioPlayableData _audioData = new AudioPlayableData(_audioConfigurationData[i].GetAudioList());

			// Make sure cooldown length will never exceed available audio
			int _cooldownLength = m_gameConfiguration.GetAudioCooldownLength();
			if (_cooldownLength > _audioConfigurationData.Length - 1)
			{
				_cooldownLength = _audioConfigurationData.Length - 1;
			}

			_audioData.Initialize(_cooldownLength);
			m_audioDataList.Add(_audioData);
		}

		// Setup Event Listener
		GameEventSystem.AddListener<OnPressedButtonEvent>(OnButtonPressedEvent);
	}

	private void OnDestroy()
	{
		GameEventSystem.RemoveListener<OnPressedButtonEvent>(OnButtonPressedEvent);
	}

	public void OnButtonPressedEvent(GameEvent p_event)
	{
		OnPressedButtonEvent _eventCast = (OnPressedButtonEvent) p_event;
		PlayAudio(_eventCast.ButtonIndex);
	}

	public void PlayAudio(int index) 
	{
		if (index >= m_audioDataList.Count) 
		{
			return;
		}

		AudioClip _randomClip = RandomizerHelper.GetRandomClip(m_audioDataList[index].ClipsReadyList, m_audioDataList[index].ClipsCooldownQueue);
		m_audioSourceComponent.PlayOneShot(_randomClip);
	}
}
