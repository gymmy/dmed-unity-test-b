﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class AudioButton : MonoBehaviour
{
    [SerializeField] private Text m_textComponent;
    [SerializeField] private Image m_imageComponent;

    public void SetTextContent(string p_textContent) 
    {
        m_textComponent.text = p_textContent;
    }

    public void SetColor(Color p_color) 
    {
        m_imageComponent.color = p_color;
    }
}
