﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class UIController : MonoBehaviour
{
    [Header("System References")]
    [SerializeField] private MainGameConfiguration m_gameConfiguration;
    [SerializeField] private AudioButtonController m_buttonController;

    [Header("UI References")]
    [SerializeField] private AudioButton m_buttonPrefab;
    [SerializeField] private Transform m_buttonTransformParent;

    private void Awake()
    {
        Initialize();
    }

    private void Initialize() 
    {
        if (m_gameConfiguration == null) 
        {
            Debug.LogError("[UIMANAGER] Please assign a game config.");
        }

        if (m_buttonController == null) 
        {
            m_buttonController = GetComponent<AudioButtonController>();
        }

        int _lengthOfGameAudio = m_gameConfiguration.GetAudioTypeCount();
        AudioButton[] _buttons = new AudioButton[_lengthOfGameAudio];
        for (int i = 0; i < _lengthOfGameAudio; ++i) 
        {
            AudioButton _newButton = Instantiate(m_buttonPrefab.gameObject, m_buttonTransformParent).GetComponent<AudioButton>();
            _newButton.transform.localScale = Vector3.one;
            string _buttonString = ASCIIConverter.GetColumnName(i);
            _newButton.SetTextContent(_buttonString);
            _buttons[i] = _newButton;
           
        }

        m_buttonController.Initialize(_buttons);
    }
}
