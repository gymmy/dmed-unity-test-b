﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

/// <summary>
/// Controller in charge of assigning events to UI buttons
/// </summary>
public class AudioButtonController : MonoBehaviour
{
    private class AudioButtonSlot 
    {
        public readonly AudioButton ButtonReference;
        public int AssignedIndex;
       
        public AudioButtonSlot(AudioButton p_buttonReference, int p_initialIndex) 
        {
            ButtonReference = p_buttonReference;
            AssignedIndex = p_initialIndex;
        }
    }

    [SerializeField] private Button m_swapButton;

    private AudioButton[] m_audioButtons;
    private AudioButtonSlot[] m_audioButtonSlots;

    private int GetSwitchIndex(int p_currentIndex, int p_maxIndex)
    {
        p_currentIndex = p_currentIndex > 0 ? p_currentIndex - 1 : p_maxIndex - 1;
        return p_currentIndex;
    }

    public void Initialize(AudioButton[] p_buttons) 
    {
        m_audioButtons = p_buttons;

        if (m_audioButtons.Length == 0 || m_audioButtons == null)
        {
            Debug.LogError("[BUTTONMANAGER] Please assign audio button references");
            return;
        }

        if (m_swapButton == null)
        {
            Debug.LogError("[BUTTONMANAGER] Please assign swap button reference");
            return;
        }

        m_audioButtonSlots = new AudioButtonSlot[m_audioButtons.Length];

        for (int i = 0; i < m_audioButtons.Length; ++i)
        {
            AudioButtonSlot _slot = new AudioButtonSlot(m_audioButtons[i], i);
            m_audioButtonSlots[i] = _slot;
        }

        SwitchButtons();
    }

    public void SwitchButtons() 
    {
        AssignIndexToAudioButtons(m_audioButtonSlots);
    }

    private void AssignIndexToAudioButtons(AudioButtonSlot[] p_audioButtonSlots) 
    {
        for (int i = 0; i < p_audioButtonSlots.Length; ++i) 
        {
            int _itrNewIndex = p_audioButtonSlots[i].AssignedIndex;
            p_audioButtonSlots[i].AssignedIndex = GetSwitchIndex(_itrNewIndex, p_audioButtonSlots.Length);

            Button _itrButton = p_audioButtonSlots[i].ButtonReference.GetComponent<Button>();

            _itrButton.onClick.RemoveAllListeners();
            _itrButton.onClick.AddListener(() => {
                OnPressedButtonEvent _pressedButtonEvent = new OnPressedButtonEvent(this, _itrNewIndex);
                GameEventSystem.RaiseEvent<OnPressedButtonEvent>(_pressedButtonEvent);
            });
        }
    }
}
