﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ASCIIConverter
{
    public static string GetColumnName(int index)
    {
        const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        string value = "";

        if (index >= letters.Length)
            value += letters[index / letters.Length - 1];

        value += letters[index % letters.Length];

        return value;
    }
}
