﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RandomizerHelper
{
	/// <summary>
	/// Works by transferring recently played audio list element into a queue that enqueues each time a random is requested.
	/// When the enqeueued object is NOT null, it transfers the confiscated object back to the randomizable list.
	/// </summary>
	public static T GetRandomClip<T>(List<T> m_activeList, Queue<T> m_inactiveQueue)
	{
		int _activeAudioListCount = m_activeList.Count - 1;

		int _random = Random.Range(0, _activeAudioListCount);
		T _randomizedElement = m_activeList[_random];

		m_activeList[_random] = m_activeList[_activeAudioListCount];
		m_activeList[_activeAudioListCount] = _randomizedElement;
		m_inactiveQueue.Enqueue(_randomizedElement);
		m_activeList.RemoveAt(_activeAudioListCount);

		T _dequeue = m_inactiveQueue.Dequeue();
		if (_dequeue != null)
		{
			m_activeList.Add(_dequeue);
		}

		return _randomizedElement;
	}
}
