﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameConfiguration", menuName = "ScriptableObjects/CreateGameConfiguration", order = 1)]
public class MainGameConfiguration : ScriptableObject
{
    [SerializeField] private int m_audioCooldownLength;
    [SerializeField] private AudioConfigurationData[] m_audioConfigurations;

    public void GetAudioConfigurationDatas(out AudioConfigurationData[] p_container) 
    {
        p_container = m_audioConfigurations;
    }

    public int GetAudioTypeCount()
    {
        return m_audioConfigurations.Length;
    }

    public int GetAudioCooldownLength()
    {
        return m_audioCooldownLength;
    }
}
